﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FX_CLIPS
{
    STEPS,
}

[System.Serializable]
public struct ClipsByName
{
    public FX_CLIPS name;
    public AudioClip clip;
}

public class AudioManager : MonoBehaviour
{
    public AudioSource fxSource;
    static public AudioManager Instance;
    public List<ClipsByName> clips;

    private void Awake()
    {
        Instance = this;
    }

    public void PlayFx(FX_CLIPS clipName)
    {
        AudioClip c = clips.Find(x => x.name == clipName).clip;
        fxSource.PlayOneShot(c);
    }
}
