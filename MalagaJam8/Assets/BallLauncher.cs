﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BallLauncher : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerBall")
        {
            PlayerBall pb = collision.GetComponentInChildren<PlayerBall>();
            if (pb.CanBounce)
            {
                collision.transform.position = transform.position;
                collision.GetComponentInChildren<PlayerBall>().Init(transform.GetChild(0).up);
            }
            else
            {
                pb.Explode();
            }
        }
    }
}
