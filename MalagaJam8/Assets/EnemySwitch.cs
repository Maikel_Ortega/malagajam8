﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySwitch : Switch
{
    public List<Enemy> enemiesSubscribed;
    int enemiesAlive;

    private void Awake()
    {
        enemiesAlive = enemiesSubscribed.Count;
        foreach (var item in enemiesSubscribed)
        {
            item.OnDeath += Item_OnDeath;
        }
    }

    private void Item_OnDeath(Enemy e)
    {
        e.OnDeath -= Item_OnDeath;
        enemiesAlive--;
        if (enemiesAlive == 0)
        {
            Activate();
        }
    }

    public override void Activate()
    {
        base.Activate();
    }
}
