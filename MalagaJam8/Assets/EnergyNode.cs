﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyNode : MonoBehaviour
{
    public Sprite sprOn;
    public Sprite sprOff;
    public SpriteRenderer mSpriteRenderer;

    public void SetState(bool on)
    {
        mSpriteRenderer.sprite = on ? sprOn : sprOff;
    }
}
