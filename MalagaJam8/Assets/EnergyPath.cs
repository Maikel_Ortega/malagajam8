﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyPath : MonoBehaviour
{
    List<EnergyNode> nodes;
    public EnergySwitch energySwitch;

    private void Awake()
    {
        nodes = new List<EnergyNode>(GetComponentsInChildren<EnergyNode>());
        energySwitch.OnSwitchActivated += EnergySwitch_OnSwitchActivated;
    }

    private void EnergySwitch_OnSwitchActivated(Switch s)
    {
        energySwitch.OnSwitchActivated -= EnergySwitch_OnSwitchActivated;
        SetState(true);
    }
    
    

    public void SetState(bool on)
    {
        foreach (var item in nodes)
        {
            item.SetState(on);
        }
    }
}
