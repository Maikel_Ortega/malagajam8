﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardFloor : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerFeet")
        {
            GameController.Instance.player.OnHazardFloor(this);
        }
    }
}
