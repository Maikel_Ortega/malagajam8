﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactSwitch : Switch
{
    public Damageable mDamageable;
    public Animator mAnimator;

    private void Start()
    {
        mDamageable.OnDamage += MDamageable_OnDamage;
    }

    private void MDamageable_OnDamage(Damageable d, float dmg)
    {
        mAnimator.SetTrigger("zap");
        Debug.Log("Damaged impact switch with " + dmg + "energy");        
        Activate();        
    }

    public override void Activate()
    {
        Debug.Log("Activated energy switch");
        base.Activate();
    }
}
