﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Animator mAnimator;
    bool clicked = false;
    bool allowClickes = false;


    void Start()
    {
        Invoke("AllowClicks", 2f);
    }

    void AllowClicks()
    {
        allowClickes = true;
    }

	void Update ()
    {
        if (!clicked && allowClickes && Input.GetButtonDown("Fire1"))
        {
            clicked = true;
            mAnimator.SetTrigger("Play");
            Invoke("LoadLevel", 2.0f);
        }
	}

    void LoadLevel()
    {
        SceneManager.LoadScene("MAIN");
    }
}
