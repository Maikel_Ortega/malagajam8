﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public FollowPlayer followCamera;
    public Room currentRoom;
    
    static public CameraManager Instance;

    private void Awake()
    {
        Instance = this;
    }


    public void SetCameraTarget(Transform t)
    {
        followCamera.SetTarget(t);
    }

    public void FreeCamera()
    {
        followCamera.SetTarget(null);
    }

    public void StartTransition()
    {
        followCamera.StartTransition();
    }

    public void SetRoom(Room newRoom)
    {
        currentRoom = newRoom;
    }

    public Rect GetCurrentRoomBounds()
    {        
        Rect r = currentRoom.cameraBounds;        
        r.x += currentRoom.transform.position.x - r.width/2;
        r.y += currentRoom.transform.position.y - r.height/ 2;
        return r;
    }
}
