﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour
{
    public delegate void DamageableDelegate(Damageable d, float dmg);
    public event DamageableDelegate OnDamage;
    public List<string> damageTag;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (damageTag.Contains(collision.tag))
        {
            DamageDealer dd = collision.GetComponent<DamageDealer>();

            if (OnDamage != null)
            {
                OnDamage(this, dd.GetDamage()); 
            }
        }        
    }
}
