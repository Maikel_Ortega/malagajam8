﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Vector2 teleportPoint;
    public Room targetRoom;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Teleport(GameController.Instance.player);
        }
    }

    void Teleport(PlayerLogic p)
    {
        p.TeleportPlayer(teleportPoint);
        CameraManager.Instance.SetRoom(targetRoom);
        GameController.Instance.SetRoomPoint(teleportPoint);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(teleportPoint, 0.1f);
    }
}
