﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameSwitch : Switch
{
    public float energyNeeded = 25;
    public float currentEnergy = 0;
    public Damageable mDamageable;
    public Animator mAnimator;
    public List<EnergyPanel> mPanels;

    private void Start()
    {
        mDamageable.OnDamage += MDamageable_OnDamage;
    }

    private void MDamageable_OnDamage(Damageable d, float dmg)
    {
        mAnimator.SetTrigger("zap");
        Debug.Log("Damaged energy switch with " + dmg + "energy");
        currentEnergy += dmg;

        if (currentEnergy >= energyNeeded)
        {
            Activate();
            mDamageable.OnDamage -= MDamageable_OnDamage;
            mAnimator.SetTrigger("empty");
            foreach (var item in mPanels)
            {
                item.SetLevel(1);
            }
        }
        else
        {
            foreach (var item in mPanels)
            {
                item.SetLevel(currentEnergy / energyNeeded);
            }            
        }
    }

    public override void Activate()
    {
        Debug.Log("Activated endgameSwitch");        
        base.Activate();
        GameController.Instance.OnWin();
    }
}
