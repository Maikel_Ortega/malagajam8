﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Damageable mDamageable;

    public int maxHp;
    private int _hp;
    public bool destroyOnDeath;
    public GameObject explosionPrefab;

    public delegate void EnemyDelegate(Enemy e);
    public event EnemyDelegate OnDeath;

    protected virtual void Start()
    {
        mDamageable.OnDamage += MDamageable_OnDamage;
        _hp = maxHp;
    }

    private void MDamageable_OnDamage(Damageable d, float dmg)
    {
        Damage(Mathf.RoundToInt(dmg));
    }

    private void Damage(int amount)
    {
        _hp -= amount;
        if (_hp <= 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {
        mDamageable.OnDamage -= MDamageable_OnDamage;
        Instantiate(explosionPrefab, this.transform.position, transform.rotation);
        if (OnDeath != null)
            OnDeath(this);
        if(destroyOnDeath)
            Destroy(this.gameObject);
    }

    public bool IsPlayerClose(float distance)
    {
        return Vector2.Distance(GameController.Instance.player.transform.position, transform.position) < distance;
    }
}
