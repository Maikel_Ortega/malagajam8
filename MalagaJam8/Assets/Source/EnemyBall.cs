﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBall : DamageDealer
{
    public bool CanBounce = false;
    public float maxTimeToLive = 0.5f;
    public float damage = 3;
    public float speed = 10f;
    Vector3 currentVelocity;
    public float ttl;
    public GameObject explosionPrefab;

    public void Init(Vector2 direction)
    {
        this.currentVelocity = direction * speed;
        ttl = maxTimeToLive;
    }

    private void Update()
    {
        transform.position += currentVelocity * Time.deltaTime;
        ttl -= Time.deltaTime;
        if (ttl < 0)
        {
            Explode();
        }
    }

    void Explode()
    {
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }

    public override float GetDamage()
    {
        return damage;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Enemy")
        {
            Explode();
        }
    }
}
