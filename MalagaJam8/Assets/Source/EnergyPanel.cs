﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyPanel : MonoBehaviour
{
    public List<Sprite> panelLevels;
    public SpriteRenderer spr;

    public void SetLevel(float value)
    {
        float normalized = Mathf.Lerp(0, panelLevels.Count-1, value);
        spr.sprite = panelLevels[Mathf.FloorToInt(normalized)];
    }
}
