﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOutPanel : MonoBehaviour
{
    public float fadeInTime = 1f;
    public float fadeOutTime = 1f;
    public Image fadeOutImage;


    IEnumerator FadeCoroutine(float seconds, float targetAlpha, System.Action callback)
    {
        float originAlpha = fadeOutImage.color.a;
        float maxSeconds = seconds;
        while (seconds > 0)
        {
            float newAlpha = Mathf.Lerp(originAlpha, targetAlpha, (maxSeconds - seconds) / maxSeconds);
            Color c = fadeOutImage.color;
            c.a = newAlpha;
            fadeOutImage.color = c;
            seconds -= Time.deltaTime;
            yield return null;
        }
        callback.Invoke();
    }

    public void FadeOut(System.Action callback)
    {
        StartCoroutine(FadeCoroutine(fadeOutTime, 1f, callback));
    }

    public void FadeIn(System.Action callback)
    {
        StartCoroutine(FadeCoroutine(fadeInTime, 0f, callback));
    }
}
