﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform target;
    public Rect cameraInnerRect;
    public Rect cameraViewRect;
    public float cameraSpeed;
    public float transitionCameraSpeed;
    public float gameCameraSpeed;


    StateMachine<FollowPlayer> mFsm;
    State<FollowPlayer> stFollow;
    State<FollowPlayer> stTransition;

    private void Awake()
    {
        stFollow = new CamStFollow();
        stTransition = new CamStTransition();
        mFsm = new StateMachine<FollowPlayer>(this, stFollow);
    }


    void LateUpdate()
    {
        mFsm.DoUpdate();
    }

    public void CheckFollowTarget()
    {
        if (target != null)
        {
            FollowTarget();
        }
    }

    public  void CheckBounds()
    {
        if (TargetOutsideRoomBounds())
        {
            SetPositionInBounds();
        }
    }

    void FollowTarget()
    {
        if (TargetOutsideOfCameraInnerRect())
        {
            Vector3 dir = (target.position - transform.position);
            dir.z = 0;
            Vector3 velocity = dir * Time.deltaTime * cameraSpeed;            
            transform.position += velocity;           
        }
    }

    public void OnTransitionEnd()
    {
        mFsm.ChangeState(stFollow);
    }

    public void StartTransition()
    {
        mFsm.ChangeState(stTransition);
    }

    public bool TargetOutsideOfCameraInnerRect()
    {
        return !cameraInnerRect.Contains(transform.InverseTransformPoint(target.position));
    }

    bool TargetOutsideRoomBounds()
    {
        if (CameraManager.Instance == null)
            return false;

        //cameraViewRect.Overlaps(currentRoomBounds))
        Rect r = CameraManager.Instance.GetCurrentRoomBounds();

        Rect worldViewRect = cameraViewRect;
        worldViewRect.x += transform.position.x - cameraViewRect.width/2;
        worldViewRect.y += transform.position.y - cameraViewRect.height / 2;

        return !RectContainsRect(r,worldViewRect);
    }

    bool RectContainsRect(Rect a, Rect b)
    {
        bool res = false;
        Vector2 bCornerTL = new Vector2(b.xMin, b.yMax);
        Vector2 bCornerTR = new Vector2(b.xMax, b.yMax);
        Vector2 bCornerBL = new Vector2(b.xMin, b.yMin);
        Vector2 bCornerBR = new Vector2(b.xMax, b.yMin);

        if (a.Contains(bCornerTL) &&
            a.Contains(bCornerTR) &&
            a.Contains(bCornerBL) &&
            a.Contains(bCornerBR))
        {
            res = true;
        }
        return res;
    }

    //Get the nearest in-bounds position for this transform
    void SetPositionInBounds()
    {
        Rect worldViewRect = cameraViewRect;
        worldViewRect.x += transform.position.x - cameraViewRect.width / 2;
        worldViewRect.y += transform.position.y - cameraViewRect.height / 2;

        Rect r = CameraManager.Instance.GetCurrentRoomBounds();
        Vector2 offset = Vector2.zero;

        if (r.xMax < worldViewRect.xMax)
        {
            offset.x = -(Mathf.Abs(r.xMax - worldViewRect.xMax));
        }
        else if (r.xMin > worldViewRect.xMin)
        {
            offset.x = (Mathf.Abs(r.xMin - worldViewRect.xMin));
        }

        if (r.yMax < worldViewRect.yMax)
        {
            offset.y = -(Mathf.Abs(r.yMax -worldViewRect.yMax));
        }
        else if (r.yMin > worldViewRect.yMin)
        {
            offset.y = (Mathf.Abs(r.yMin - worldViewRect.yMin));
        }
        transform.position += new Vector3(offset.x, offset.y, 0);
    }

    public void SetTarget(Transform t)
    {
        target = t;
    }


    private void OnDrawGizmos()
    {
        if (TargetOutsideOfCameraInnerRect())
            Gizmos.color = Color.red;
        else
            Gizmos.color = Color.green;

        Gizmos.DrawWireCube(transform.position, new Vector3(cameraInnerRect.width, cameraInnerRect.height, 0));
        Rect b = cameraInnerRect;


        b.x += transform.position.x ;
        b.y += transform.position.y ;

        Vector2 bCornerTL = new Vector2(b.xMin, b.yMax);
        Vector2 bCornerTR = new Vector2(b.xMax, b.yMax);
        Vector2 bCornerBL = new Vector2(b.xMin, b.yMin);
        Vector2 bCornerBR = new Vector2(b.xMax, b.yMin);

        Gizmos.DrawWireSphere(bCornerTL, 0.1f);
        Gizmos.DrawWireSphere(bCornerTR, 0.1f);
        Gizmos.DrawWireSphere(bCornerBL, 0.1f);
        Gizmos.DrawWireSphere(bCornerBR, 0.1f);

        if (TargetOutsideRoomBounds())
            Gizmos.color = Color.red;
        else
            Gizmos.color = Color.green;

        Rect b2 = cameraViewRect;

        b2.x += transform.position.x - b2.width/2;
        b2.y += transform.position.y - b2.height/2;

        bCornerTL = new Vector2(b2.xMin, b2.yMax);
        bCornerTR = new Vector2(b2.xMax, b2.yMax);
        bCornerBL = new Vector2(b2.xMin, b2.yMin);
        bCornerBR = new Vector2(b2.xMax, b2.yMin);

        Gizmos.DrawWireSphere(bCornerTL, 0.1f);
        Gizmos.DrawWireSphere(bCornerTR, 0.1f);
        Gizmos.DrawWireSphere(bCornerBL, 0.1f);
        Gizmos.DrawWireSphere(bCornerBR, 0.1f);

        Gizmos.DrawWireCube(transform.position, new Vector3(cameraViewRect.width, cameraViewRect.height, 0));
    }
}
