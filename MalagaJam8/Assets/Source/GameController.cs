﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    public PlayerLogic player;

    public float maxTime = 240f;
    public float currentTime = 240f;
    public float timeLostPerSecond = 1f;

    public FadeOutPanel fadeOutMgr;
    public Image barImg;

    StateMachine<GameController> mFsm;
    State<GameController> stIntro;
    State<GameController> stGame;
    State<GameController> stDeath;

    public Room initialRoom;
    public Vector2 spawnPoint;
    public Vector2 lastRoomPoint;

    public GameObject introPrefab;
    public GameObject instantiatedIntro;
    public Vector2 introOffset;
    public float introAnimationSeconds = 2f;

    internal void SetRoomPoint(Vector2 teleportPoint)
    {
        lastRoomPoint = teleportPoint;
    }

    void Awake()
    {
        Instance = this;
        stIntro = new GCStIntro();
        stGame= new GCStGame();
        stDeath = new GCStDeath();
        mFsm = new StateMachine<GameController>(this, stIntro);
        mFsm.ChangeState( stIntro);
	}

    public float GetCurrentTime()
    {
        return currentTime;
    }

    protected void SetTime(float value)
    {       
        currentTime = Mathf.Clamp( value, 0 , maxTime);
        UpdateBar();
        if (currentTime == 0 && mFsm.CurrentState() == stGame)
        {
            OnTimeDepleted();
        }
    }

    void UpdateBar()
    {
        barImg.fillAmount =  GetCurrentTime()/maxTime;
    }

    public void ResetTime()
    {
        SetTime(maxTime);
    }

    public void DepleteTime(float amount)
    {
        SetTime(GetCurrentTime() - amount);        
    }

    private void Update()
    {
        mFsm.DoUpdate();    
    }

    public void CheckTime()
    {
        DepleteTime(Time.deltaTime * timeLostPerSecond);
    }

    public void InitIntro()
    {

        fadeOutMgr.FadeIn(OnIntroFadeIn);
        player.Init(spawnPoint);
        player.gameObject.SetActive(false);
        lastRoomPoint = spawnPoint;
        ResetTime();
    }

    void OnIntroFadeIn()
    {
        Debug.Log("INTRO FADED IN");
        CameraManager.Instance.SetRoom(initialRoom);
        CameraManager.Instance.StartTransition();
        instantiatedIntro = Instantiate(introPrefab, spawnPoint + introOffset, Quaternion.identity);
        StartCoroutine(WaitAndStartGame(introAnimationSeconds));
    }

    IEnumerator WaitAndStartGame(float introSeconds)
    {
        yield return new WaitForSeconds(introSeconds);
        Destroy(instantiatedIntro);
        player.gameObject.SetActive(true);
        player.OnIntroFinished();
        mFsm.ChangeState(stGame);
    }

    public void OnWin()
    {
        Debug.Log("YOU WIN");
        UnityEngine.SceneManagement.SceneManager.LoadScene("Start_Scene");
    }

    void OnTimeDepleted()
    {
        mFsm.ChangeState(stDeath);
        Debug.Log("DEATH");
    }

    public void InitDeath()
    {
        fadeOutMgr.FadeOut(OnDeathFadeOut);
    }

    void OnDeathFadeOut()
    {
        mFsm.ChangeState(stIntro);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(spawnPoint, 0.25f);
    }
}
