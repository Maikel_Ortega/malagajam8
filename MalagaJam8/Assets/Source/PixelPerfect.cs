﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelPerfect : MonoBehaviour
{
	void LateUpdate ()
    {
        transform.localPosition = Vector3.zero;
        transform.position = new Vector3(Mathf.Round(transform.position.x * 16f) / 16, Mathf.Round(transform.position.y * 16f) / 16, Mathf.Round(transform.position.z * 16f) / 16);
    }
}
