﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerBall : DamageDealer
{
    public bool CanBounce = false;
    public float maxTimeToLive = 0.5f;
    public float damage = 3;
    public float speed = 10f;
    Vector3 currentVelocity;
    public GameObject explosionPrefab;
    public float ttl;
    
    

    public void Init(Vector2 direction)
    {
        this.currentVelocity = direction * speed;
        ttl = maxTimeToLive;
    }

    private void Update()
    {
        transform.position += currentVelocity * Time.deltaTime;
        ttl -= Time.deltaTime;
        if (ttl < 0)
        {
            Explode();
        }
    }

    public void Explode()
    {
        Instantiate(explosionPrefab, transform.position, transform.rotation); 
        Destroy(this.gameObject);
    }

    public override float GetDamage()
    {
        return damage;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if (!collision.tag.Contains("Player"))
        if (collision.tag.Contains("Solid") || collision.tag.Contains("Enemy"))
        {
            Explode();
        }
    }
}
