﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DIRECTIONS
{
    UP,
    RIGHT,
    DOWN,
    LEFT,
    NEUTRAL,
}

public enum CHARGE_LEVELS
{
    BASIC,
    ONE,
    TWO,
}

[System.Serializable]
public struct TransformsByDirection
{
    public DIRECTIONS dir;
    public Transform t;
}

[System.Serializable]
public struct ShotsByLevel
{
    public CHARGE_LEVELS level;
    public PlayerBall proyectile;
    public float energyCost;
}

public class PlayerLogic : MonoBehaviour
{
    public float speed = 5;
    public DIRECTIONS direction = DIRECTIONS.UP;
    public Animator mAnimator;
    Vector2 lastInput;
    Vector3 movement;
    public BoxCollider2D mCollider;
    public ContactFilter2D collisionFilter;
    Vector3 originColliderLocalPos;
    Collider2D[] results;
    bool collided = false;
    float timeCharging = 0;
    float levelOneSeconds = 0.5f;
    float levelMaxSeconds = 1.5f;
    public SpriteRenderer atk1Renderer;
    public SpriteRenderer atk2Renderer;
    public List<ShotsByLevel> shotPrefabs;
    public List<TransformsByDirection> bulletSpawnPoints;

    public float maxHazardTime = 1f;
    float hazardTime = 0f;
    public float hazardDamage = 25;
    public Damageable mDamageable;

    public StateMachine<PlayerLogic> mFsm;
    public State<PlayerLogic> StIntro;
    public State<PlayerLogic> StGame;
    public State<PlayerLogic> StDamage;
    public State<PlayerLogic> StHazard;


    private void Awake()
    {
        originColliderLocalPos = mCollider.transform.localPosition;
        StIntro     = new PlayerStIntro();
        StGame      = new PlayerStGame();
        StHazard    = new PlayerStHazard();
        StDamage    = new PlayerStDamage();

        mFsm = new StateMachine<PlayerLogic>(this, StIntro);
        mFsm.ChangeState(StIntro);
        mDamageable.OnDamage += MDamageable_OnDamage;
    }

    private void MDamageable_OnDamage(Damageable d, float dmg)
    {
        GameController.Instance.DepleteTime(dmg);
        Hit();
    }

    private void Start()
    {        
    }

    public void Init(Vector2 position)
    {
        transform.position = position;
    }

    public void OnIntroFinished()
    {
        Debug.Log("PLAYER On Intro Finished");
        mFsm.ChangeState(StGame);
    }

    void Update ()
    {
        mFsm.DoUpdate();        
	}

    internal void UpdateGame()
    {
        collided = false;
        movement = Vector3.zero;
        CheckMovement();
        CheckDirection();
        CheckCollisions();
        CheckChargeShot();
        Move();
    }

    internal void CheckHazardTime()
    {
        hazardTime -= Time.deltaTime;
        if (hazardTime < 0)
        {
            RoomRespawn();
            mFsm.ChangeState(StGame);
            GameController.Instance.DepleteTime(hazardDamage);
        }
    }

    void RoomRespawn()
    {
        TeleportPlayer(GameController.Instance.lastRoomPoint);
        mFsm.ChangeState(StGame);
    }

    public void TeleportPlayer(Vector2 point, bool cameraTransition = true)
    {
        transform.position = new Vector3(point.x, point.y, 0);
        if(cameraTransition)
            CameraManager.Instance.StartTransition();
    }

    void CheckMovement()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        lastInput.x = x;
        lastInput.y = y;

        movement = new Vector3(x, y,0) * speed * Time.deltaTime;
        mAnimator.SetFloat("speed",movement.magnitude);

    }

    void Move()
    {
        transform.position += movement;
    }

    void CheckDirection()
    {
        DIRECTIONS newDir = FromVector2Dir(lastInput);

        if (newDir != DIRECTIONS.NEUTRAL && direction != newDir)
        {
            direction = newDir;      

            mAnimator.SetBool(direction.ToString(), true);
            foreach (var item in System.Enum.GetValues(typeof(DIRECTIONS)))
            {
                if ((DIRECTIONS)item != direction && (DIRECTIONS)item != DIRECTIONS.NEUTRAL)
                {
                    mAnimator.SetBool(item.ToString(), false);
                }
            }
        }
    }

    void CheckCollisions()
    {
        mCollider.transform.localPosition += Vector3.right * movement.x;

        Collider2D hit = Physics2D.OverlapBox(mCollider.transform.position, mCollider.size,0, collisionFilter.layerMask);
        if (hit != null)
        {
            collided = true;
            movement.x = 0;
        }
        mCollider.transform.localPosition = originColliderLocalPos;

        mCollider.transform.localPosition += Vector3.up * movement.y;

        hit = Physics2D.OverlapBox(mCollider.transform.position, mCollider.size, 0, collisionFilter.layerMask);
        if (hit != null)
        {
            collided = true;
            movement.y = 0;
        }

        mCollider.transform.localPosition = originColliderLocalPos;
    }

    void CheckChargeShot()
    {
        bool held =  Input.GetButton("Fire1");
        CHARGE_LEVELS level = SecondsToChargeLevel(timeCharging);

        if (held)
        {
            timeCharging += Time.deltaTime;
            ShowCharge(level);
        }
        else if(timeCharging > 0)
        {
            Shot(level);
            timeCharging = 0;
            ShowCharge(CHARGE_LEVELS.BASIC);
        }
    }

    CHARGE_LEVELS SecondsToChargeLevel(float seconds)
    {
        if (seconds < levelOneSeconds)
        {
            return CHARGE_LEVELS.BASIC;
        }
        else if (seconds < levelMaxSeconds)
        {
            return CHARGE_LEVELS.ONE;
        }
        else
        {
            return CHARGE_LEVELS.TWO;
        }
    }

    void Shot(CHARGE_LEVELS level)
    {
        ShotsByLevel sbl = this.shotPrefabs.Find(x => x.level == level);
        PlayerBall bPrefab = sbl.proyectile;
        TransformsByDirection tbd = bulletSpawnPoints.Find(x => x.dir == direction);

        Transform spawner = tbd.t;
        ParticleSystem ps = spawner.GetComponentInChildren<ParticleSystem>();
        ps.Play();

        PlayerBall bullet = Instantiate(bPrefab, spawner.position, Quaternion.identity) as PlayerBall;
        bullet.Init(FromDir2Vector(this.direction));

        GameController.Instance.DepleteTime(sbl.energyCost);
    }
    

    IEnumerator WaitAndChangeToGameState(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        mFsm.ChangeState(StGame);
    }



    void ShowCharge(CHARGE_LEVELS level)
    {
        switch (level)
        {
            case CHARGE_LEVELS.BASIC:
                atk1Renderer.gameObject.SetActive(false);
                atk2Renderer.gameObject.SetActive(false);
                break;
            case CHARGE_LEVELS.ONE:
                atk1Renderer.gameObject.SetActive(true);
                atk2Renderer.gameObject.SetActive(false);
                break;
            case CHARGE_LEVELS.TWO:
                atk1Renderer.gameObject.SetActive(false);
                atk2Renderer.gameObject.SetActive(true);
                break;
        }
    }

    DIRECTIONS FromVector2Dir(Vector2 v)
    {
        return FromVector2Dir(v.x, v.y);
    }

    Vector2 FromDir2Vector(DIRECTIONS d)
    {
        switch (d)
        {
            case DIRECTIONS.UP:
                return new Vector2(0, 1);
                break;
            case DIRECTIONS.RIGHT:
                return new Vector2(1, 0);
                break;
            case DIRECTIONS.DOWN:
                return new Vector2(0, -1);
                break;
            case DIRECTIONS.LEFT:
                return new Vector2(-1, 0);
                break;
        }   
        return new Vector2(0, 0);
    }

        DIRECTIONS FromVector2Dir(float x, float y)
    {
        DIRECTIONS d  = DIRECTIONS.NEUTRAL;
        if ((x != 0 && y == 0)||(x == 0 && y != 0))
        {
            if (x > 0)
            {
                d = DIRECTIONS.RIGHT;
            }
            else if (x < 0)
            {
                d = DIRECTIONS.LEFT;
            }
            else if (y > 0)
            {
                d = DIRECTIONS.UP;
            }
            else if (y < 0)
            {
                d = DIRECTIONS.DOWN;
            }
        }

        return d;
    }

    internal void OnHazardFloor(HazardFloor hazardFloor)
    {
        this.hazardTime = maxHazardTime;
        TeleportPlayer(hazardFloor.transform.position+Vector3.up*0.35f);
        mFsm.ChangeState(StHazard);
        mAnimator.Play("hazard");
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = (collided ? Color.red : Color.green);

        Gizmos.DrawWireCube(mCollider.transform.position, mCollider.size);
    }

    void Hit()
    {
        mAnimator.Play("Hit");
        mFsm.ChangeState(StDamage);
        StartCoroutine(WaitAndChangeToGameState(0.5f));
    }    
}