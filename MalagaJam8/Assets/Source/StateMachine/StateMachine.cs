﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StateMachine<T> 
{
	T owner;
	State<T> currentState;
	State<T> previousState;
	State<T> globalState;


	public State<T> CurrentState() { return currentState;}
	public State<T> GlobalState() { return globalState;}
	public State<T> PreviousState() { return previousState;}

	public StateMachine (T owner, State<T> currentState, State<T> previousState = null, State<T> globalState = null)
	{
		this.owner = owner;
		this.currentState = currentState;
		this.previousState = previousState;
		this.globalState = globalState; 
	}

	public void DoUpdate()
	{
		if( globalState != null) {globalState.Execute(owner);}
		if( currentState != null ){currentState.Execute(owner);}
	}

	

	public void ChangeState(State<T> nextState)
	{
		previousState = currentState;
		currentState.Exit(owner);
		currentState = nextState;
		currentState.Enter(owner);
	}

	public void RevertToPreviousState()
	{
		ChangeState(previousState);
	}

	public bool IsInState(State<T> s)
	{
		return currentState.Equals(s);
	}
}
