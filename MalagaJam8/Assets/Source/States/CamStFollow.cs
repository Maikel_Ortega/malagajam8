﻿using UnityEngine;
using System.Collections;

public class CamStFollow : State<FollowPlayer> 
{
	public override void Enter (FollowPlayer owner)
	{
        owner.cameraSpeed = owner.gameCameraSpeed;
	}

	public override void Execute (FollowPlayer owner)
	{
        owner.CheckFollowTarget();
        owner.CheckBounds();
	}

	public override void Exit (FollowPlayer owner)
	{
	}
}
