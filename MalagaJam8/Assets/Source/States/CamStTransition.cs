﻿using UnityEngine;
using System.Collections;

public class CamStTransition : State<FollowPlayer> 
{
	public override void Enter (FollowPlayer owner)
	{
        owner.cameraSpeed = owner.transitionCameraSpeed;
    }

	public override void Execute (FollowPlayer owner)
	{
        owner.CheckFollowTarget();
        if (!owner.TargetOutsideOfCameraInnerRect())
        {
            owner.OnTransitionEnd();
        }
	}

	public override void Exit (FollowPlayer owner)
	{
	}
}
