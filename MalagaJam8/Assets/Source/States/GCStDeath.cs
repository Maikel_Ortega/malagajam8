﻿using UnityEngine;
using System.Collections;

public class GCStDeath : State<GameController> 
{
	public override void Enter (GameController owner)
	{
        owner.InitDeath();
	}

	public override void Execute (GameController owner)
	{
	}

	public override void Exit (GameController owner)
	{
	}
}
