﻿using UnityEngine;
using System.Collections;

public class GCStGame : State<GameController> 
{
	public override void Enter (GameController owner)
	{        
	}

	public override void Execute (GameController owner)
	{
        owner.CheckTime();
	}

	public override void Exit (GameController owner)
	{
	}
}
