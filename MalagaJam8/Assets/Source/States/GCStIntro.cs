﻿using UnityEngine;
using System.Collections;

public class GCStIntro : State<GameController> 
{
	public override void Enter (GameController owner)
	{
        owner.InitIntro();
	}

	public override void Execute (GameController owner)
	{
	}

	public override void Exit (GameController owner)
	{
	}
}
