﻿using UnityEngine;
using System.Collections;

public class PlayerStGame : State<PlayerLogic> 
{
	public override void Enter (PlayerLogic owner)
	{
	}

	public override void Execute (PlayerLogic owner)
	{
        owner.UpdateGame();
	}

	public override void Exit (PlayerLogic owner)
	{
	}
}
