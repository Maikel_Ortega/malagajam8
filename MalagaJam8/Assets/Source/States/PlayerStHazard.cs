﻿using UnityEngine;
using System.Collections;

public class PlayerStHazard : State<PlayerLogic> 
{
	public override void Enter (PlayerLogic owner)
	{
	}

	public override void Execute (PlayerLogic owner)
	{
        owner.CheckHazardTime();
	}

	public override void Exit (PlayerLogic owner)
	{
	}
}
