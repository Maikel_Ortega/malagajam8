﻿using UnityEngine;
using System.Collections;

public class TurretStDead : State<TurretEnemy> 
{
	public override void Enter (TurretEnemy owner)
	{
        owner.LaunchDeadAnimation();
	}

	public override void Execute (TurretEnemy owner)
	{
        
	}

	public override void Exit (TurretEnemy owner)
	{
	}
}
