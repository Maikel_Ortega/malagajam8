﻿using UnityEngine;
using System.Collections;

public class TurretStShoot : State<TurretEnemy> 
{
	public override void Enter (TurretEnemy owner)
	{
        owner.ShootAtPlayer();
	}

	public override void Execute (TurretEnemy owner)
	{
	}

	public override void Exit (TurretEnemy owner)
	{
	}
}
