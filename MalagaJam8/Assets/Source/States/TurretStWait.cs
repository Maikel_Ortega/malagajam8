﻿using UnityEngine;
using System.Collections;

public class TurretStWait : State<TurretEnemy> 
{
	public override void Enter (TurretEnemy owner)
	{
        owner.GetRandomWaitTime();
	}

	public override void Execute (TurretEnemy owner)
	{
        owner.CheckWait();
        if (owner.waitingTime < 0)
        {
            owner.ChangeToShootState();
        }
	}

	public override void Exit (TurretEnemy owner)
	{
	}
}
