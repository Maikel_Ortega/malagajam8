﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    public delegate void SwitchDelegate(Switch s);
    public event SwitchDelegate OnSwitchActivated;

    public virtual void Activate()
    {
        if (OnSwitchActivated != null)
        {
            OnSwitchActivated(this);
        }
    }
}
