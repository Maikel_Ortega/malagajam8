﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchableItem : MonoBehaviour
{
    public bool startsOn = false;
    public string turnOnTrigger = "open";
    public string turnOffTrigger = "close";
    public Switch mSwitch;
    public bool currentlyOn;

    void Start ()
    {
        mSwitch.OnSwitchActivated += OnSwitchActivated;
        currentlyOn = startsOn;
	}

    protected virtual void OnSwitchActivated(Switch s)
    {
        Debug.Log("Switch message received!");        
        this.GetComponent<Animator>().SetTrigger(currentlyOn ? turnOffTrigger : turnOnTrigger);
        currentlyOn = !currentlyOn;
    }
}
