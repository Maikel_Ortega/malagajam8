﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretEnemy : Enemy
{
    StateMachine<TurretEnemy> mFsm;

    State<TurretEnemy> StUnderground;
    State<TurretEnemy> StShoot;
    State<TurretEnemy> StWait;
    State<TurretEnemy> StDead;

    public float maxWaitTime = 5f;
    public float minWaitTime = 3f;
    internal float waitingTime = 5;

    public Animator mAnimator;

    public EnemyBall bullet;
    Coroutine shootCoroutine;

    protected override void Start()
    {
        base.Start();
        StShoot = new TurretStShoot();
        StWait = new TurretStWait();
        StDead = new TurretStDead();
        mFsm = new StateMachine<TurretEnemy>(this, StWait);
        mFsm.ChangeState(StWait);
    }

    private void Update()
    {
        mFsm.DoUpdate();
    }

    public override void Die()
    {
        mFsm.ChangeState(StDead);
        base.Die();
    }

    internal void GetRandomWaitTime()
    {
        waitingTime = UnityEngine.Random.Range(minWaitTime, maxWaitTime);
    }

    internal void CheckWait()
    {
        waitingTime -= Time.deltaTime;        
    }

    internal void ChangeToShootState()
    {
        mFsm.ChangeState(StShoot);
    }

    internal void LaunchShootAnimation()
    {
        mAnimator.SetTrigger("shot");
    }

    internal void LaunchDeadAnimation()
    {
        if (shootCoroutine != null)
            StopCoroutine(shootCoroutine);
        mAnimator.Play("Death");
        GetComponentInChildren<Collider2D>().enabled = false;
        GetComponentInChildren<ZSort>().enabled = false;
        GetComponentInChildren<SpriteRenderer>().sortingLayerName = "Bg";
        GetComponentInChildren<SpriteRenderer>().sortingOrder = 1;
    }

    internal void ShootAtPlayer()
    {
        if (IsPlayerClose(11f))
        {
            LaunchShootAnimation();
            if (shootCoroutine != null)
                StopCoroutine(shootCoroutine);
            shootCoroutine = StartCoroutine(ShootAfterSeconds(0.5f));
        }
        else
        { 
            mFsm.ChangeState(StWait);
        }
    }

    IEnumerator ShootAfterSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        PlayerLogic p = GameController.Instance.player;
        Shoot(p.transform.position - transform.position);
        mFsm.ChangeState(StWait);
    }
    

    void Shoot(Vector2 direction)
    {
        EnemyBall eb = Instantiate(bullet, transform.position, transform.rotation) as EnemyBall;
        eb.Init(direction.normalized);
    }
}
