﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZSort : MonoBehaviour
{
    SpriteRenderer spr;
    public Transform zPivot;
    public int offset;
    private void Awake()
    {
        spr = this.GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (zPivot == null)
        {
            zPivot = this.transform;
        }
        spr.sortingOrder = - Mathf.RoundToInt(zPivot.position.y*10) + offset;
    }
}
