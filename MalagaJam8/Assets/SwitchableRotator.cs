﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SpritesByRotation
{
    public float rotation;
    public Sprite spr;
}

public class SwitchableRotator : SwitchableItem
{
    public float angle = 90;
    public float currentAngle;
    public List<SpritesByRotation> spritesByRotation;
    public SpriteRenderer mainSpriteRenderer;

    protected override void OnSwitchActivated(Switch s)
    {
        Rotate();
    }

    void Rotate()
    {
        transform.Rotate(Vector3.back * angle, Space.Self);

        currentAngle = (360+(currentAngle - 90)) % 360;
        mainSpriteRenderer.sprite = spritesByRotation.Find(x => x.rotation == currentAngle).spr;
    }
}
